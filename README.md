# Websosanh's Machine Learning Literature Research Repository

This repository stores paper lists and summarization of those papers.

# Workflow

1. Decide paper list in the daily meeting. Generally one paper per person,
with an exception of popular papers that requires everyone to know.

2. Each folder is a conference (e.g. `2017_CoNLL` is the 2017 proceeding
of CoNLL conference). Within this folder, there is one file named
`00_paper_list.md` logging paper reading progress and links to each paper.

3. Read the paper in about 1-2 hours, take note for writing a summary. The
summary can be written using [StackEdit](https://stackedit.io/app#) and
stored in the corresponding folder of the conference.

4. Later, decide if a paper is worth implementing or looking into the code.
